package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class RankRobotData implements PropertyReader
{
	public int id;
	public String name;
	public String icon;
	public int level;
	public int level1;
	public int cardid1;
	public int level2;
	public int cardid2;
	public int level3;
	public int cardid3;
	public int level4;
	public int cardid4;
	public int level5;
	public int cardid5;
	public int level6;
	public int cardid6;
	public int power;

	public String[] cards;
	
	private static List<RankRobotData> data=new ArrayList<RankRobotData>();
	
	@Override
	public void addData()
	{
		cards=new String[6];
		cards[0]=cardid1+"-"+level1;
		cards[1]=cardid2+"-"+level2;
		cards[2]=cardid3+"-"+level3;
		cards[3]=cardid4+"-"+level4;
		cards[4]=cardid5+"-"+level5;
		cards[5]=cardid6+"-"+level6;
		
		data.add(this);
	}

	@Override
	public void parse(String[] ss)
	{

	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static List<RankRobotData> getDatas()
	{
		return data;
	}
	
	public int getUnitSkillId()
	{
		List<Integer> cardIds=new ArrayList<Integer>();
		for(String s:cards)
		{
			String[] ss=s.split("-");
			int cardId=StringUtil.getInt(ss[0]);
			int level=StringUtil.getInt(ss[1]);
			if(cardId>0 && level>0)
			{
				cardIds.add(cardId);
			}
		}
		List<Integer> unitIds=UnitSkillData.getUnitSkillIds(cardIds);
		int result=0;
		int power=0;
		for(int unitId:unitIds)
		{
			UnitSkillData usd=UnitSkillData.getData(unitId);
			if(result==0)
			{
				result=unitId;
			}
			else
			{
				if(usd.power>power)
				{
					result=unitId;
					power=usd.power;
				}
			}
		}
		return result;
	}
	
}
