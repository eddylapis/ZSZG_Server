package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class SackData implements PropertyReader
{
	public int id;
	public String name;
	public int cost;
	public int timesmin;
	public int timesmax;
	public int uselevel;
	public int viprequest;
	public int switchpoint;
	public List<String> rewards;
	
	private static HashMap<Integer, SackData> data =new HashMap<Integer, SackData>();
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location=0;
		id =StringUtil.getInt(ss[location]);
		name =StringUtil.getString(ss[location+1]);
		cost =StringUtil.getInt(ss[location+2]);
		timesmin =StringUtil.getInt(ss[location+3]);
		timesmax =StringUtil.getInt(ss[location+4]);
		uselevel =StringUtil.getInt(ss[location+5]);
		viprequest =StringUtil.getInt(ss[location+6]);
		switchpoint =StringUtil.getInt(ss[location+7]);
		rewards =new ArrayList<String>();
		for(int i=0;i<10;i++)
		{
			location =8+i*4;
			int rewardtype =StringUtil.getInt(ss[location]);
			String reward =StringUtil.getString(ss[location+1]);
			int pro =StringUtil.getInt(ss[location+2]);
			int probabilityswitch =StringUtil.getInt(ss[location+3]);
			if(rewardtype !=0)
			{
				String str =rewardtype+"-"+reward+"-"+pro+"-"+probabilityswitch;
				rewards.add(str);
			}
		}
		addData();
	}

	@Override
	public void resetData() 
	{
		data.clear();
	}
	
	public static SackData getSackData(int index)
	{
		return data.get(index);
	}

}
