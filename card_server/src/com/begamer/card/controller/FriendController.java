package com.begamer.card.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.begamer.card.cache.Cache;
import com.begamer.card.cache.MailThread;
import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.Constant;
import com.begamer.card.common.util.RequestUtil;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.common.util.binRead.FriendCostData;
import com.begamer.card.json.FriendElement;
import com.begamer.card.json.command2.BuyFriendJson;
import com.begamer.card.json.command2.BuyResultJson;
import com.begamer.card.json.command2.FriendApplyJson;
import com.begamer.card.json.command2.FriendCancelApplyJson;
import com.begamer.card.json.command2.FriendJson;
import com.begamer.card.json.command2.FriendMyApplyJson;
import com.begamer.card.json.command2.FriendProcessApplyJson;
import com.begamer.card.json.command2.FriendRefreshJson;
import com.begamer.card.json.command2.FriendRemoveJson;
import com.begamer.card.json.command2.FriendResultJson;
import com.begamer.card.json.command2.FriendSearchJson;
import com.begamer.card.log.ErrorLogger;
import com.begamer.card.log.PlayerLogger;
import com.begamer.card.model.pojo.Friend;
import com.begamer.card.model.pojo.LogBuy;
import com.begamer.card.model.pojo.Player;

public class FriendController extends AbstractMultiActionController
{
	private static final Logger logger=Logger.getLogger(FriendController.class);
 	private static Logger playlogger=PlayerLogger.logger;
 	private static Logger errorlogger=ErrorLogger.logger;
 	
 	public ModelAndView friendList(HttpServletRequest request , HttpServletResponse response)
 	{
 		try{
			//检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if(checkResult != null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/***********************************修改部分 start************************************/
			//获取参数
			FriendJson fj=JSONObject.toJavaObject(jsonObject, FriendJson.class);
			FriendResultJson frj=new FriendResultJson();
			//获取请求的数据
			PlayerInfo pi=Cache.getInstance().getPlayerInfo(fj.getPlayerId(),request.getSession());
			if(pi!=null)
			{
				//校验模块解锁
				int errorCode =0;
				if(!UiController.checkMode(21, pi))
				{
					errorCode =56;
				}
				if(errorCode ==0)
				{
					int type=fj.t;
					int index=fj.i;
					List<Friend> friends=pi.getFriends();
					switch (type)
					{
					case 0://查看好友列表
						frj.list=createFriends4(friends);
						frj.buyNum=pi.player.getBuyFriendTimes();
						playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|查看好友列表");
						break;
					case 1://收体力
						if(index<0 || index>=friends.size())
						{
							frj.errorCode=45;
						}
						else
						{
							Friend f=friends.get(index);
							//好友状态:0未赠不可收,1已删除,2未赠可收,3未赠已收,4已赠不可收,5已赠可收,6已赠已收
							if(f.getState()==2 || f.getState()==5)
							{
								if(pi.player.getPowerTimes()>=Cache.maxPowerTimes)
								{
									frj.errorCode=47;
								}
								else
								{
									if(f.getState()==2)
									{
										f.setState(3);
									}
									if(f.getState()==5)
									{
										f.setState(6);
									}
									pi.player.addPower(Cache.friendPower,false);
									pi.player.addPowerTime();
									frj.list=createFriends4(friends);
									frj.buyNum=pi.player.getBuyFriendTimes();
									playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|接收好友赠送的体力："+f.getFriendId());
								}
							}
							else
							{
								frj.errorCode=45;
							}
						}
						break;
					case 2://送体力
						
						if(index<0 || index>=friends.size())
						{
							frj.errorCode=46;
						}
						else
						{
							//好友状态:0未赠不可收,1已删除,2未赠可收,3未赠已收,4已赠不可收,5已赠可收,6已赠已收
							Friend f=friends.get(index);
							if(f.getState()==0 || f.getState()==2 || f.getState()==3)
							{
								if(f.getState()==0)
								{
									f.setState(4);
								}
								if(f.getState()==2)
								{
									f.setState(5);
								}
								if(f.getState()==3)
								{
									f.setState(6);
								}
								frj.list=createFriends4(friends);
								frj.buyNum=pi.player.getBuyFriendTimes();
								Cache.getInstance().sendPower(f.getFriendId(), f.getPlayerId());
								playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|赠送好友体力："+f.getFriendId());
							}
							else
							{
								frj.errorCode=46;
							}
						}
						break;
					}
					frj.power=pi.player.getPower();
				}
				else
				{
					frj.errorCode =errorCode;
				}
			}
			else
			{
				frj.errorCode=-3;
			}
			Cache.recordRequestNum(frj);
			String msg = JSON.toJSONString(frj);
			/***********************************修改部分 end**************************************/
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e) 
		{
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
 	}
 	
 	/**删除好友**/
 	public ModelAndView deleteFriend(HttpServletRequest request , HttpServletResponse response)
 	{
 		try{
			//检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if(checkResult != null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/***********************************修改部分 start************************************/
			//获取参数
			FriendRemoveJson rfj=JSONObject.toJavaObject(jsonObject, FriendRemoveJson.class);
			FriendResultJson frj=new FriendResultJson();
			//获取请求的数据
			PlayerInfo pi=Cache.getInstance().getPlayerInfo(rfj.getPlayerId(),request.getSession());
			if(pi!=null)
			{
				List<Friend> friends=pi.getFriends();
				int index=rfj.i;
				if(friends==null || friends.size()==0 || index<0 || index>=friends.size())
				{
					frj.errorCode=34;
				}
				else
				{
					Friend f=friends.get(index);
					Cache.getInstance().removeFriend(f.getPlayerId(), f.getFriendId());
					Cache.getInstance().removeFriend(f.getFriendId(), f.getPlayerId());
					friends=pi.getFriends();
					frj.list=createFriends(friends,0,true);
					frj.buyNum=pi.player.getBuyFriendTimes();
					playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|删除好友："+f.getFriendId());
				}
			}
			else
			{
				frj.errorCode=-3;
			}
			Cache.recordRequestNum(frj);
			String msg = JSON.toJSONString(frj);
			/***********************************修改部分 end**************************************/
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e) 
		{
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
 	}
 	
 	/**刷新未申请为好友的玩家,一次最多5个**/
 	public ModelAndView refreshFriend(HttpServletRequest request , HttpServletResponse response)
 	{
 		try{
			//检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if(checkResult != null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/***********************************修改部分 start************************************/
			//获取参数
			FriendRefreshJson rfj=JSONObject.toJavaObject(jsonObject, FriendRefreshJson.class);
			FriendResultJson frj=new FriendResultJson();
			//获取请求的数据
			PlayerInfo pi=Cache.getInstance().getPlayerInfo(rfj.getPlayerId(),request.getSession());
			if(pi!=null)
			{
				//刷新时间判断
				if(rfj.t==1 && System.currentTimeMillis()-pi.lastRefreshTime<15*1000)
				{
					frj.errorCode=35;
				}
				else
				{
					List<Integer> refreshFriendIds=pi.getRefreshFriends(rfj.t==0);
					frj.list=createFriends2(refreshFriendIds,pi.player.getId(),rfj.t==1);
					frj.buyNum=pi.player.getBuyFriendTimes();
					playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|刷新未申请好友的玩家");
				}
			}
			else
			{
				frj.errorCode=-3;
			}
			Cache.recordRequestNum(frj);
			String msg = JSON.toJSONString(frj);
			/***********************************修改部分 end**************************************/
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e) 
		{
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
 	}
 	
 	/**申请好友,最多10个**/
 	public ModelAndView applyFriend(HttpServletRequest request , HttpServletResponse response)
 	{
 		try{
			//检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if(checkResult != null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/***********************************修改部分 start************************************/
			//获取参数
			FriendApplyJson faj=JSONObject.toJavaObject(jsonObject, FriendApplyJson.class);
			FriendResultJson frj=new FriendResultJson();
			//获取请求的数据
			PlayerInfo pi=Cache.getInstance().getPlayerInfo(faj.getPlayerId(),request.getSession());
			if(pi!=null)
			{
				boolean canApply=true;
				int friendId=0;
				List<Friend> applys=Cache.getInstance().getMyApplys(pi.player.getId());
				if(faj.s==1)
				{
					if(pi.lastSearch==0)
					{
						frj.errorCode=37;
					}
					else
					{
						friendId=pi.lastSearch;
						for(Friend f:applys)
						{
							if(f.getFriendId()==friendId)
							{
								canApply=false;
								break;
							}
						}
					}
				}
				else if(faj.s==0)
				{
					List<Integer> list=pi.lastRefreshList;
					if(list==null || list.size()<=faj.i)
					{
						frj.errorCode=37;
					}
					else
					{
						friendId=list.get(faj.i);
						for(Friend f:applys)
						{
							if(f.getFriendId()==friendId)
							{
								canApply=false;
								break;
							}
						}
					}
				}
				else if(faj.s==2)
				{
					friendId=faj.i;
				}
				if(frj.errorCode==0 && canApply)
				{
					if(applys.size()>=Constant.MaxApplyFriendNum)
					{
						//删掉最早的一个申请
						Friend firstF=null;
						for(Friend apply:applys)
						{
							if(firstF==null)
							{
								firstF=apply;
							}
							else
							{
								if(apply.getAddDate().compareTo(firstF.getAddDate())<0)
								{
									firstF=apply;
								}
							}
						}
						Cache.getInstance().removeApply(firstF);
					}
					Friend f=new Friend();
					f.setPlayerId(pi.player.getId());
					f.setFriendId(friendId);
					f.setAddDate(StringUtil.getDateTime(System.currentTimeMillis()));
					Cache.getInstance().addFriendApply(f);
					playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|申请成为"+friendId+"的好友");
				}
			}
			else
			{
				frj.errorCode=-3;
			}
			Cache.recordRequestNum(frj);
			String msg = JSON.toJSONString(frj);
			/***********************************修改部分 end**************************************/
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e) 
		{
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
 	}
 	
 	/**我的申请/申请我的**/
 	public ModelAndView myApply(HttpServletRequest request , HttpServletResponse response)
 	{
 		try{
			//检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if(checkResult != null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/***********************************修改部分 start************************************/
			//获取参数
			FriendMyApplyJson fmj=JSONObject.toJavaObject(jsonObject, FriendMyApplyJson.class);
			FriendResultJson frj=new FriendResultJson();
			//获取请求的数据
			PlayerInfo pi=Cache.getInstance().getPlayerInfo(fmj.getPlayerId(),request.getSession());
			if(pi!=null)
			{
				List<Friend> list=null;
				if(fmj.t==0)
				{
					list=Cache.getInstance().getMyApplys(pi.player.getId());
					playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|查看我的申请");
				}
				else
				{
					list=Cache.getInstance().getApplyMes(pi.player.getId());
					playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|查看申请我的");
				}
				frj.list=createFriends(list,2,fmj.t==0);
				frj.buyNum=pi.player.getBuyFriendTimes();
			}
			else
			{
				frj.errorCode=-3;
			}
			Cache.recordRequestNum(frj);
			String msg = JSON.toJSONString(frj);
			/***********************************修改部分 end**************************************/
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e) 
		{
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
 	}
 	
 	/**取消申请**/
 	public ModelAndView cancelApply(HttpServletRequest request , HttpServletResponse response)
 	{
 		try{
			//检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if(checkResult != null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/***********************************修改部分 start************************************/
			//获取参数
			FriendCancelApplyJson fcj=JSONObject.toJavaObject(jsonObject, FriendCancelApplyJson.class);
			FriendResultJson frj=new FriendResultJson();
			//获取请求的数据
			PlayerInfo pi=Cache.getInstance().getPlayerInfo(fcj.getPlayerId(),request.getSession());
			if(pi!=null)
			{
				List<Friend> list=Cache.getInstance().getMyApplys(pi.player.getId());
				if(fcj.i<0 || fcj.i>=list.size())
				{
					frj.errorCode=38;
				}
				else
				{
					Friend f=list.get(fcj.i);
					Cache.getInstance().removeApply(f);
					list=Cache.getInstance().getMyApplys(pi.player.getId());
					frj.list=createFriends(list,2,true);
					frj.buyNum=pi.player.getBuyFriendTimes();
					playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|取消申请");
				}
			}
			else
			{
				frj.errorCode=-3;
			}
			Cache.recordRequestNum(frj);
			String msg = JSON.toJSONString(frj);
			/***********************************修改部分 end**************************************/
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e) 
		{
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
 	}
 	
 	/**搜索玩家**/
 	public ModelAndView search(HttpServletRequest request , HttpServletResponse response)
 	{
 		try{
			//检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if(checkResult != null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/***********************************修改部分 start************************************/
			//获取参数
			FriendSearchJson fsj=JSONObject.toJavaObject(jsonObject, FriendSearchJson.class);
			FriendResultJson frj=new FriendResultJson();
			//获取请求的数据
			PlayerInfo pi=Cache.getInstance().getPlayerInfo(fsj.getPlayerId(),request.getSession());
			if(pi!=null)
			{
				String userName=fsj.name;
				if(userName==null || "".equals(userName))
				{
					frj.errorCode=42;
				}
				else if(userName.trim().equals(pi.player.getName()))
				{
					frj.errorCode=43;
				}
				else
				{
					Player p=Cache.getInstance().searchPlayer(userName);
					if(p==null)
					{
						frj.errorCode=42;
					}
					else
					{
						pi.lastSearch=p.getId();
						int state=1;
						if(pi.isFriend(p.getId()))
						{
							state=0;
						}
						else
						{
							List<Friend> applys=Cache.getInstance().getMyApplys(pi.player.getId());
							for(Friend f:applys)
							{
								if(f.getFriendId()==p.getId())
								{
									state=2;
									break;
								}
							}
						}
						List<FriendElement> list=new ArrayList<FriendElement>();
						list.add(new FriendElement(p,state));
						frj.list=list;
					}
				}
				frj.buyNum=pi.player.getBuyFriendTimes();
				playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|查看我的申请");
			}
			else
			{
				frj.errorCode=-3;
			}
			Cache.recordRequestNum(frj);
			String msg = JSON.toJSONString(frj);
			/***********************************修改部分 end**************************************/
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e) 
		{
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
 	}
 	
 	/**购买好友上限**/
 	public ModelAndView buyFriend(HttpServletRequest request , HttpServletResponse response)
 	{
 		try{
			//检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if(checkResult != null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/***********************************修改部分 start************************************/
			//获取参数
			BuyFriendJson bfj=JSONObject.toJavaObject(jsonObject, BuyFriendJson.class);
			BuyResultJson bfrj=new BuyResultJson();
			//获取请求的数据
			PlayerInfo pi=Cache.getInstance().getPlayerInfo(bfj.getPlayerId(),request.getSession());
			if(pi!=null)
			{
				int buyFriendTimes=pi.player.getBuyFriendTimes();
				FriendCostData fd=FriendCostData.getData(buyFriendTimes+1);
				if(fd==null)
				{
					bfrj.errorCode=97;
				}
				else
				{
					//钻石
					if(fd.type==1)
					{
						if(pi.player.getTotalCrystal()<fd.cost)
						{
							bfrj.errorCode=71;
						}
						else
						{
							int[] crystals=pi.player.removeCrystal(fd.cost);
							MailThread.getInstance().addLogbuy(LogBuy.createLogBuy(pi.player.getId(), "购买好友上限",crystals));
							pi.player.addBuyFriendTimes();
							bfrj.buyTimes=pi.player.getBuyFriendTimes();
							bfrj.gold=pi.player.getGold();
							bfrj.crystal=pi.player.getTotalCrystal();
						}
					}
					//金币
					else
					{
						if(pi.player.getGold()<fd.cost)
						{
							bfrj.errorCode=89;
						}
						else
						{
							pi.player.removeGold(fd.cost);
							pi.player.addBuyFriendTimes();
							bfrj.buyTimes=pi.player.getBuyFriendTimes();
							bfrj.gold=pi.player.getGold();
							bfrj.crystal=pi.player.getTotalCrystal();
						}
					}
				}
			}
			else
			{
				bfrj.errorCode=-3;
			}
			Cache.recordRequestNum(bfrj);
			String msg = JSON.toJSONString(bfrj);
			/***********************************修改部分 end**************************************/
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e) 
		{
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
 	}
 	
 	/**处理申请**/
 	public ModelAndView processApply(HttpServletRequest request , HttpServletResponse response)
 	{
 		try{
			//检验密钥
			ModelAndView checkResult = RequestUtil.check(request);
			if(checkResult != null)
			{
				return checkResult;
			}
			//获取json
			JSONObject jsonObject = RequestUtil.getJsonObject(request);
			/***********************************修改部分 start************************************/
			//获取参数
			FriendProcessApplyJson fpj=JSONObject.toJavaObject(jsonObject, FriendProcessApplyJson.class);
			FriendResultJson frj=new FriendResultJson();
			//获取请求的数据
			PlayerInfo pi=Cache.getInstance().getPlayerInfo(fpj.getPlayerId(),request.getSession());
			if(pi!=null)
			{
				//同意
				if(fpj.t==1)
				{
					List<Friend> list=Cache.getInstance().getApplyMes(pi.player.getId());
					if(fpj.i<0 || fpj.i>=list.size())
					{
						frj.errorCode=41;
					}
					else
					{
						Friend f=list.get(fpj.i);
						if(!Cache.getInstance().canAddFriend(f.getFriendId()))
						{
							frj.errorCode=39;
						}
						else if(!Cache.getInstance().canAddFriend(f.getPlayerId()))
						{
							frj.errorCode=40;
						}
						else
						{
							//删除双方的申请
							Cache.getInstance().removeApply(f);
							List<Friend> list2=Cache.getInstance().getMyApplys(pi.player.getId());
							for(Friend f2:list2)
							{
								if(f2.getFriendId()==f.getPlayerId())
								{
									Cache.getInstance().removeApply(f2);
									break;
								}
							}
							Cache.getInstance().addFriend(f.getPlayerId(), f.getFriendId());
							Cache.getInstance().addFriend(f.getFriendId(), f.getPlayerId());
							list=Cache.getInstance().getApplyMes(pi.player.getId());
							frj.list=createFriends(list,2,false);
							frj.buyNum=pi.player.getBuyFriendTimes();
							playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|同意申请"+f.getFriendId());
						}
					}
				}
				//拒绝
				else
				{
					List<Friend> list=Cache.getInstance().getApplyMes(pi.player.getId());
					if(fpj.i<0 || fpj.i>=list.size())
					{
						frj.errorCode=41;
					}
					else
					{
						Friend f=list.get(fpj.i);
						Cache.getInstance().removeApply(f);
						list=Cache.getInstance().getApplyMes(pi.player.getId());
						frj.list=createFriends(list,2,false);
						frj.buyNum=pi.player.getBuyFriendTimes();
						playlogger.info("|区服："+Cache.getInstance().serverId+"|玩家："+pi.player.getId()+"|"+pi.player.getName()+"|等级："+pi.player.getLevel()+"|拒绝申请"+f.getFriendId());
					}
				}
			}
			else
			{
				frj.errorCode=-3;
			}
			Cache.recordRequestNum(frj);
			String msg = JSON.toJSONString(frj);
			/***********************************修改部分 end**************************************/
			logger.info("msg:"+msg);
			RequestUtil.setResponseMsg(request, msg);
			return new ModelAndView("/user/result.vm");
		}
		catch (Exception e) 
		{
			errorlogger.error(this.getClass().getName()+"->"+Thread.currentThread().getStackTrace()[1].getMethodName()+"():",e);
			RequestUtil.removeKey(request);
			String newKey = RequestUtil.setNewKey(request);
			RequestUtil.setResult(request, newKey);
		}
		return new ModelAndView("/user/result.vm");
 	}
 	
 	/**
 	 * 
 	 * lt@2014-3-3 下午03:14:06
 	 * @param friends
 	 * @param state 0已经是好友,1未申请,2已申请
 	 * @return
 	 */
 	private static List<FriendElement> createFriends(List<Friend> friends,int state,boolean setFriendId)
 	{
 		if(friends==null || friends.size()==0)
 		{
 			return null;
 		}
 		List<FriendElement> list=new ArrayList<FriendElement>();
 		for(int k=0;k<friends.size();k++)
 		{
 			Friend f=friends.get(k);
 			if(setFriendId)
 			{
 				FriendElement fe=new FriendElement(f.getFriendId(),state);
 	 			list.add(fe);
 			}
 			else
 			{
 				FriendElement fe=new FriendElement(f.getPlayerId(),state,f.getAddDate());
 	 			list.add(fe);
			}
 		}
 		return list;
 	}
 	
 	/**
 	 * 
 	 * lt@2014-3-3 下午03:14:06
 	 * @param friends
 	 * @param state 0已经是好友,1未申请,2已申请
 	 * @return
 	 */
 	private static List<FriendElement> createFriends2(List<Integer> friendIds,int playerId,boolean isRefresh)
 	{
 		if(!isRefresh)
 		{
 			return createFriends3(friendIds,playerId);
 		}
 		
 		if(friendIds==null || friendIds.size()==0)
 		{
 			return null;
 		}
 		List<FriendElement> list=new ArrayList<FriendElement>();
 		for(int k=0;k<friendIds.size();k++)
 		{
 			int friendId=friendIds.get(k);
 			FriendElement fe=new FriendElement(friendId,1);
 			list.add(fe);
 		}
 		return list;
 	}
 	
 	private static List<FriendElement> createFriends3(List<Integer> friendIds,int playerId)
 	{
 		if(friendIds==null || friendIds.size()==0)
 		{
 			return null;
 		}
 		List<FriendElement> list=new ArrayList<FriendElement>();
 		List<Friend> applys=Cache.getInstance().getMyApplys(playerId);
 		for(int k=0;k<friendIds.size();k++)
 		{
 			int friendId=friendIds.get(k);
 			/**0已经是好友,1未申请,2已申请**/
 			int state=1;
 			for(Friend f:applys)
 			{
 				if(f.getFriendId()==friendId)
 				{
 					state=2;
 					break;
 				}
 			}
 			FriendElement fe=new FriendElement(friendId,state);
 			list.add(fe);
 		}
 		return list;
 	}
 	
 	/**
 	 * 
 	 * lt@2014-3-3 下午03:14:06
 	 * @param friends
 	 * @param state 0已经是好友,1未申请,2已申请
 	 * @return
 	 */
 	private static List<FriendElement> createFriends4(List<Friend> friends)
 	{
 		if(friends==null || friends.size()==0)
 		{
 			return null;
 		}
 		List<FriendElement> list=new ArrayList<FriendElement>();
 		for(int k=0;k<friends.size();k++)
 		{
 			Friend f=friends.get(k);
			FriendElement fe=new FriendElement(f.getFriendId(),f.getState());
 			list.add(fe);
 		}
 		return list;
 	}
}
