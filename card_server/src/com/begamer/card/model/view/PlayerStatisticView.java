package com.begamer.card.model.view;

public class PlayerStatisticView {

	private int id;
	private String name;
	private int level;
	private int crystal;
	private int crystalPay;
	//排行榜
	private int pkRank;
	//vip等级
	private int vipLevel;
	//总战斗力
	private int battlePower;
	//本月签到次数
	private int signTimes;
	//上次签到时间,yyyy-MM-dd HH:mm:ss
	private String lastSignTime;
	
	public static PlayerStatisticView createPlayerStatisticView(int id,String name,int level,int crystal,int crystalPay,int pkRank,int vipLevel,int battlePower,int signTimes,String lastSignTime)
	{
		PlayerStatisticView playerStatisticView = new PlayerStatisticView();
		playerStatisticView.setId(id);
		playerStatisticView.setName(name);
		playerStatisticView.setLevel(level);
		playerStatisticView.setCrystal(crystal);
		playerStatisticView.setCrystalPay(crystalPay);
		playerStatisticView.setPkRank(pkRank);
		playerStatisticView.setVipLevel(vipLevel);
		playerStatisticView.setBattlePower(battlePower);
		playerStatisticView.setSignTimes(signTimes);
		playerStatisticView.setLastSignTime(lastSignTime);
		return playerStatisticView;
	}	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public int getLevel()
	{
		return level;
	}
	public void setLevel(int level)
	{
		this.level = level;
	}
	public int getCrystal()
	{
		return crystal;
	}
	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}
	public int getPkRank()
	{
		return pkRank;
	}
	public void setPkRank(int pkRank)
	{
		this.pkRank = pkRank;
	}
	public int getVipLevel()
	{
		return vipLevel;
	}
	public void setVipLevel(int vipLevel)
	{
		this.vipLevel = vipLevel;
	}
	public int getBattlePower()
	{
		return battlePower;
	}
	public void setBattlePower(int battlePower)
	{
		this.battlePower = battlePower;
	}
	public int getSignTimes()
	{
		return signTimes;
	}
	public void setSignTimes(int signTimes)
	{
		this.signTimes = signTimes;
	}
	public String getLastSignTime()
	{
		return lastSignTime;
	}
	public void setLastSignTime(String lastSignTime)
	{
		this.lastSignTime = lastSignTime;
	}
	public void setCrystalPay(int crystalPay)
	{
		this.crystalPay = crystalPay;
	}
	public int getCrystalPay()
	{
		return crystalPay;
	}
	
	
}
