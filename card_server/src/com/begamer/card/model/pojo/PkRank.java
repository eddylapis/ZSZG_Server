package com.begamer.card.model.pojo;

import com.begamer.card.common.util.StringUtil;

public class PkRank
{
	public int id;
	public int playerId;
	public int playerLevel;
	public int rank;
	public int pvpAward;
	public int rankAwardType;
	public int pkNum;
	public String zeroTime;
	public int entryType;
	public int winNum;
	public String pkrecord;//type(0,挑战别人,1,别人来挑战)&id(pk玩家的id)&rank(0,排名不变,x提升或者下降至x名)&r(1胜利,2失败)&time(记录时间)
	
	public static PkRank getNewPkRank(int playerId)
	{
		PkRank pr =new PkRank();
		pr.setPlayerId(playerId);
		pr.setRank(0);
		pr.setPvpAward(0);
		pr.setRankAwardType(0);
		pr.setZeroTime(StringUtil.getDate(System.currentTimeMillis()));
		pr.setPkNum(0);
		pr.setEntryType(0);
		pr.setWinNum(0);
		pr.setPkrecord("");
		return pr;
	}
	
	public static PkRank getNewPkRank(int playerId,int playerLevel,int rank)
	{
		PkRank pr =new PkRank();
		pr.setPlayerId(playerId);
		pr.setPlayerLevel(playerLevel);
		pr.setRank(rank);
		pr.setPvpAward(0);
		pr.setRankAwardType(0);
		pr.setZeroTime(StringUtil.getDate(System.currentTimeMillis()));
		pr.setPkNum(0);
		pr.setEntryType(0);
		pr.setWinNum(0);
		pr.setPkrecord("");
		return pr;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public int getPvpAward() {
		return pvpAward;
	}
	public void setPvpAward(int pvpAward) {
		this.pvpAward = pvpAward;
	}
	public int getRankAwardType() {
		return rankAwardType;
	}
	public void setRankAwardType(int rankAwardType) {
		this.rankAwardType = rankAwardType;
	}
	public int getPkNum() {
		return pkNum;
	}
	public void setPkNum(int pkNum) {
		this.pkNum = pkNum;
	}
	public String getZeroTime() {
		return zeroTime;
	}
	public void setZeroTime(String zeroTime) {
		this.zeroTime = zeroTime;
	}
	public int getEntryType() {
		return entryType;
	}
	public void setEntryType(int entryType) {
		this.entryType = entryType;
	}

	public int getWinNum() {
		return winNum;
	}

	public void setWinNum(int winNum) {
		this.winNum = winNum;
	}

	public String getPkrecord() {
		return pkrecord;
	}

	public void setPkrecord(String pkrecord) {
		this.pkrecord = pkrecord;
	}

	public int getPlayerLevel() {
		return playerLevel;
	}

	public void setPlayerLevel(int playerLevel) {
		this.playerLevel = playerLevel;
	}

}
