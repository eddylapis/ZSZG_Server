package com.begamer.card.model.pojo;

import com.begamer.card.common.Constant;
import com.begamer.card.common.util.StringUtil;

public class SpeciaMail {
	
	private int id;
	private int type;
	private String typeInfo;
	private int value;
	private int value2;
	private String beginTime;// 格式yyyy-MM-dd
	private String endTime;// 格式yyyy-MM-dd
	private int state;// 0关闭，1开启
	private String title;
	private String content;
	private String reward1;// 格式:type&id&number
	private String reward2;
	private String reward3;
	private String reward4;
	private String reward5;
	private String reward6;
	private int gold;
	private int crystal;
	private int runeNum;
	private int power;
	private int friendNum;
	private int deleteTime;
	private String sendTime;// 格式HH:mm
	private String lastSendDate;// 上次发送日期,
	
	public static SpeciaMail createMail(int type, String typeInfo, int value, int value2, String beginTime, String endTime, String title, String content, String reward1, String reward2, String reward3, String reward4, String reward5, String reward6, int gold, int crystal, int runeNum, int power, int friendNum, String sendTime)
	{
		SpeciaMail speciaMail = new SpeciaMail();
		speciaMail.setType(type);
		speciaMail.setTypeInfo(typeInfo);
		speciaMail.setValue(value);
		speciaMail.setBeginTime(beginTime);
		speciaMail.setEndTime(endTime);
		speciaMail.setState(0);
		speciaMail.setTitle(title);
		speciaMail.setContent(content);
		speciaMail.setReward1(reward1);
		speciaMail.setReward2(reward2);
		speciaMail.setReward3(reward3);
		speciaMail.setReward4(reward4);
		speciaMail.setReward5(reward5);
		speciaMail.setReward6(reward6);
		speciaMail.setGold(gold);
		speciaMail.setCrystal(crystal);
		speciaMail.setRuneNum(runeNum);
		speciaMail.setPower(power);
		speciaMail.setFriendNum(friendNum);
		speciaMail.setDeleteTime(Constant.MailKeepTime1);
		if (type == 11)
		{
			speciaMail.setValue2(value2);
			speciaMail.setSendTime(sendTime);
		}
		if (type == 12)
		{
			speciaMail.setSendTime(sendTime);
		}
		return speciaMail;
	}
	
	/**
	 * 判断活动是否有效
	 * 
	 * @return
	 */
	public boolean isTrue(long curTime)
	{
		boolean is = false;
		String curDay = StringUtil.getDateTime(curTime);
		if (curDay.compareTo(beginTime) >= 0 && curDay.compareTo(endTime) <= 0)
		{
			if (lastSendDate == null || "".equals(lastSendDate) || curDay.compareTo(lastSendDate) > 0)
			{
				return true;
			}
		}
		return is;
	}
	
	/**
	 * 判断特殊定时邮件是否有效
	 * 
	 * @param curDate
	 * @return -1数据无效,1还未到日期,0日期中
	 */
	public int valid(long curTime)
	{
		String curDate = StringUtil.getDate(curTime);
		if (beginTime.compareTo(endTime) > 0 || curDate.compareTo(endTime) > 0)
		{
			return -1;
		}
		if (curDate.compareTo(beginTime) < 0)
		{
			return 1;
		}
		return 0;
	}
	
	/**
	 * 判断当前时间是否可发送
	 * 
	 * @param curTime
	 * @return
	 */
	public boolean canSend(long curTimeStamp)
	{
		String curDate = StringUtil.getDate(curTimeStamp);
		String curTime = StringUtil.getTime2(curTimeStamp);
		
		long timeStamp = StringUtil.getTimeStamp(curDate + " " + curTime + ":00") + 5 * 60 * 1000;
		String maxSendTime = StringUtil.getTime2(timeStamp);
		if (curTime.compareTo(sendTime) >= 0 && curTime.compareTo(maxSendTime) <= 0)
		{
			if (lastSendDate == null || "".equals(lastSendDate) || curDate.compareTo(lastSendDate) > 0)
			{
				return true;
			}
		}
		return false;
	}
	
	/** 解锁合体技特殊邮件判断发送日期 **/
	public boolean canDay(long curTime)
	{
		boolean is = false;
		String curDay = StringUtil.getDateTime(curTime);
		if (curDay.compareTo(endTime) >= 0)
		{
			return true;
		}
		return is;
	}
	
	public String getTypeInfo()
	{
		return typeInfo;
	}
	
	public void setTypeInfo(String typeInfo)
	{
		this.typeInfo = typeInfo;
	}
	
	public int getValue()
	{
		return value;
	}
	
	public void setValue(int value)
	{
		this.value = value;
	}
	
	public String getBeginTime()
	{
		return beginTime;
	}
	
	public void setBeginTime(String beginTime)
	{
		this.beginTime = beginTime;
	}
	
	public String getEndTime()
	{
		return endTime;
	}
	
	public void setEndTime(String endTime)
	{
		this.endTime = endTime;
	}
	
	public int getDeleteTime()
	{
		return deleteTime;
	}
	
	public void setDeleteTime(int deleteTime)
	{
		this.deleteTime = deleteTime;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public String getContent()
	{
		return content;
	}
	
	public void setContent(String content)
	{
		this.content = content;
	}
	
	public String getReward1()
	{
		return reward1;
	}
	
	public void setReward1(String reward1)
	{
		this.reward1 = reward1;
	}
	
	public String getReward2()
	{
		return reward2;
	}
	
	public void setReward2(String reward2)
	{
		this.reward2 = reward2;
	}
	
	public String getReward3()
	{
		return reward3;
	}
	
	public void setReward3(String reward3)
	{
		this.reward3 = reward3;
	}
	
	public String getReward4()
	{
		return reward4;
	}
	
	public void setReward4(String reward4)
	{
		this.reward4 = reward4;
	}
	
	public String getReward5()
	{
		return reward5;
	}
	
	public void setReward5(String reward5)
	{
		this.reward5 = reward5;
	}
	
	public String getReward6()
	{
		return reward6;
	}
	
	public void setReward6(String reward6)
	{
		this.reward6 = reward6;
	}
	
	public int getGold()
	{
		return gold;
	}
	
	public void setGold(int gold)
	{
		this.gold = gold;
	}
	
	public int getCrystal()
	{
		return crystal;
	}
	
	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}
	
	public int getRuneNum()
	{
		return runeNum;
	}
	
	public void setRuneNum(int runeNum)
	{
		this.runeNum = runeNum;
	}
	
	public int getPower()
	{
		return power;
	}
	
	public void setPower(int power)
	{
		this.power = power;
	}
	
	public int getFriendNum()
	{
		return friendNum;
	}
	
	public void setFriendNum(int friendNum)
	{
		this.friendNum = friendNum;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public int getType()
	{
		return type;
	}
	
	public void setType(int type)
	{
		this.type = type;
	}
	
	public int getState()
	{
		return state;
	}
	
	public void setState(int state)
	{
		this.state = state;
	}
	
	public int getValue2()
	{
		return value2;
	}
	
	public void setValue2(int value2)
	{
		this.value2 = value2;
	}
	
	public String getSendTime()
	{
		return sendTime;
	}
	
	public void setSendTime(String sendTime)
	{
		this.sendTime = sendTime;
	}
	
	public String getLastSendDate()
	{
		return lastSendDate;
	}
	
	public void setLastSendDate(long curTimeStamp)
	{
		this.lastSendDate = StringUtil.getDate(curTimeStamp);
	}
	
}
