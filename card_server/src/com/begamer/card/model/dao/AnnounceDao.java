package com.begamer.card.model.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.begamer.card.model.pojo.Announce;

public class AnnounceDao extends HibernateDaoSupport {
	Logger logger = Logger.getLogger(AnnounceDao.class);

	@SuppressWarnings("unchecked")
	public List<Announce> find()
	{
		logger.info("finding all announce instance");
		try
		{
			return getHibernateTemplate().find("from Announce a");

		}
		catch (RuntimeException e)
		{
			logger.debug("find all announce failed" + e);
			throw e;
		}
	}

	public void update(Announce announce)
	{
		logger.info("updating announce instance");
		try
		{
			getHibernateTemplate().update(announce);
			logger.info("update announce successful");
		}
		catch (RuntimeException e)
		{
			logger.debug("update announce failed" + e);
			throw e;
		}
	}

	public void save(Announce announce)
	{
		try
		{
			getHibernateTemplate().save(announce);
		}
		catch (RuntimeException e)
		{
			throw e;
		}
	}
}
