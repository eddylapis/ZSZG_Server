package com.begamer.card.model.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.begamer.card.model.pojo.TimeMail;

public class TimeMailDao extends HibernateDaoSupport {

	Logger logger = Logger.getLogger(TimeMailDao.class);

	@SuppressWarnings("unchecked")
	public List<TimeMail> getAll()
	{
		try
		{
			return getHibernateTemplate().find("from TimeMail m");
		}
		catch (RuntimeException e)
		{
			logger.debug("find all TimeMail failed!");
			throw e;
		}
	}

	public void save(TimeMail timeMail)
	{
		logger.info("save one TimeMail instance");
		try
		{
			getHibernateTemplate().save(timeMail);
			logger.info("save successful");
		}
		catch (RuntimeException e)
		{
			logger.debug("save failed" + e);
			throw e;
		}
	}

	public TimeMail one(int id)
	{
		logger.info("find one TimeMail instance");
		try
		{
			return (TimeMail) getHibernateTemplate().get(TimeMail.class, id);
		}
		catch (RuntimeException e)
		{
			logger.debug("find one TimeMail failed" + e);
			throw e;
		}
	}

	public void update(TimeMail timeMail)
	{
		logger.info("updating TimeMail instance");
		try
		{
			getHibernateTemplate().update(timeMail);
			logger.info("update TimeMail successful");
		}
		catch (RuntimeException e)
		{
			logger.debug("update TimeMail failed" + e);
			throw e;
		}
	}

	public void delete(TimeMail timeMail)
	{
		logger.info("deleting TimeMail instance");
		try
		{
			getHibernateTemplate().delete(timeMail);
			logger.info("delete TimeMail successful");
		}
		catch (RuntimeException e)
		{
			logger.debug("delete TimeMail failed" + e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<TimeMail> isOpen()
	{
		logger.info("finding isOpen TimeMail instance");
		try
		{
			return getHibernateTemplate().find("from TimeMail m where m.state = 1");
		}
		catch (RuntimeException e)
		{
			logger.debug("find failed" + e);
			throw e;
		}
	}
}
