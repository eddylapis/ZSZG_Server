package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.ShopElement;

public class RunnerUiResultJson extends ErrorJson
{
	public List<ShopElement> list;

	public List<ShopElement> getList() {
		return list;
	}

	public void setList(List<ShopElement> list) {
		this.list = list;
	}
	
	
}
