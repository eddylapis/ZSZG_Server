package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.GameBoxElement;

public class GameBoxResultJson extends ErrorJson
{
	public List<GameBoxElement> ges;

	public List<GameBoxElement> getGes() {
		return ges;
	}

	public void setGes(List<GameBoxElement> ges) {
		this.ges = ges;
	}
}
