package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;

public class ImaginationClearResultJson extends ErrorJson 
{
	/**清理获得的金币**/
	public int gold;
	/**背包空余格数**/
	public int i;
	/**碎片信息**/
	public List<PackElement> s;
	/**玩家金币数**/
	public int g;
	
	public int getGold() {
		return gold;
	}
	public void setGold(int gold) {
		this.gold = gold;
	}
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	public List<PackElement> getS() {
		return s;
	}
	public void setS(List<PackElement> s) {
		this.s = s;
	}
	public int getG() {
		return g;
	}
	public void setG(int g) {
		this.g = g;
	}
	
}
