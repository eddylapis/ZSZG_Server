package com.begamer.card.json.command2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.util.Statics;
import com.begamer.card.common.util.binRead.EquipData;
import com.begamer.card.common.util.binRead.PassiveSkillData;
import com.begamer.card.controller.UiController;
import com.begamer.card.json.CGEJson;
import com.begamer.card.json.CGPJson;
import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;
import com.begamer.card.model.pojo.Card;
import com.begamer.card.model.pojo.CardGroup;
import com.begamer.card.model.pojo.Equip;
import com.begamer.card.model.pojo.PassiveSkill;
import com.begamer.card.model.pojo.Skill;

public class CGResultJson extends ErrorJson{
	public List<PackElementJson> cs;
	public List<PackElement> ss;
	public List<CGPJson> ps;
	public List<CGEJson> es;
	public int unitId;
	
	public List<String> ics;
	public List<String> iss;
	public List<String> ips;
	public List<String> ies;
	public int unit;/**新合体技提醒       1有新提醒     0为没有**/
	
	public void transform(CardGroup cg,PlayerInfo pi)
	{
		unitId=cg.unitId;
		
		ss=new ArrayList<PackElement>();
		iss=new ArrayList<String>();
		for(int i=0;i<cg.skills.length;i++)
		{
			if(cg.skills[i]!=null)
			{
				Skill s=cg.skills[i];
				int index=pi.getSkills().indexOf(s);
				PackElement pe=Statics.createPackElement(index, s, pi);
				ss.add(pe);
				iss.add(i+"");
			}
		}
		
		ps=new ArrayList<CGPJson>();
		ips=new ArrayList<String>();
		for(int i=0;i<cg.pSkills.length;i++)
		{
			if(cg.pSkills[i]!=null)
			{
				CGPJson cgp = new CGPJson();
				List<PackElement> pes = new ArrayList<PackElement>();
				List<PassiveSkill> pss =cg.pSkills[i];
				for (PassiveSkill p : pss)
				{
					if (p==null)
					{
						pes.add(null);
					}
					else
					{
						int index=pi.getPassiveSkillls().indexOf(p);
						PackElement pe=Statics.createPackElement(index, p, pi);
						pes.add(pe);
					}
				}
				cgp.passiveSkills=pes;
				ps.add(cgp);
				ips.add(i+"");
			}
		}
		
		es=new ArrayList<CGEJson>();
		ies=new ArrayList<String>();
		for(int i=0;i<cg.equips.length;i++)
		{
			if(cg.equips[i]!=null)
			{
				CGEJson cge=new CGEJson();
				List<PackElement> pes=new ArrayList<PackElement>();
				for(Equip e:cg.equips[i])
				{
					int index=pi.getEquips().indexOf(e);
					PackElement pe=Statics.createPackElement(index, e, pi);
					pes.add(pe);
				}
				cge.equips=pes;
				es.add(cge);
				ies.add(i+"");
			}
		}
		cs=new ArrayList<PackElementJson>();
		ics=new ArrayList<String>();
		List<PackElement> pes =UiController.getElements(pi, 4);
		List<PackElement> pskills =UiController.getElements(pi, 3);
		List<PackElement> pes1 =new ArrayList<PackElement>();
		List<PackElement> pes2=new ArrayList<PackElement>();
		for(PackElement pe:pes)
		{
			if(pe !=null)
			{
				if(pe.getUse()==0)
				{
					pes2.add(pe);
				}
			}
		}
		for(PackElement pe:pskills)
		{
			if(pe !=null)
			{
				if(pe.getUse()==0)
				{
					pes1.add(pe);
				}
			}
		}
		for(int i=0;i<cg.cards.length;i++)
		{
			int type=0;
			if(cg.cards[i]!=null)
			{
				Card c=cg.cards[i];
				int index=pi.getCards().indexOf(c);
				PackElement pe=Statics.createPackElement(index, c, pi);
				PackElementJson pej =new PackElementJson();
				pej.pe =pe;
				
				/**--------判断卡位处的装备是否有空位或者是否可以提升-------**/
				//passiceskill
				if(type ==0)
				{
					List<PassiveSkill> list =cg.getPassiveSkills(i);
					if (list != null && list.size()>0)
					{
						for(int k =0;k<3;k++)
						{
							PassiveSkill p = list.get(k);
							if (p==null && pes1 !=null && pes1.size()>0)
							{
								boolean is= true;
								int num = 0;
								for (int j = 0; j < 3; j++)
								{
									PassiveSkill p2 = list.get(j);
									if (p2!=null)
									{
										PassiveSkillData psd1 = PassiveSkillData.getData(p2.getPassiveSkillId());
										for (int m = 0; m < pes1.size(); m++)
										{
											PassiveSkillData psd2 = PassiveSkillData.getData(pes1.get(m).dataId);
											if (psd1.type == psd2.type)
											{
												num++;
												if (pes1.size()<=num)
												{
													is = false;
													break;
												}
											}
										}
									}
								}
								if (k==0 && UiController.checkMode(34, pi) && is)
								{
									type = 1;
								}
								if (k==1 && UiController.checkMode(35, pi) && is)
								{
									type = 1;
								}
								if (k==2 && UiController.checkMode(36, pi) && is)
								{
									type = 1;
								}
							}
						}
					}
					else
					{
						for (int m = 0; m < 3; m++)
						{
							if (m==0 && UiController.checkMode(34, pi) && pes1.size()>0)
							{
								type = 1;
							}
							if (m==1 && UiController.checkMode(35, pi) && pes1.size()>0)
							{
								type = 1;
							}
							if (m==2 && UiController.checkMode(36, pi) && pes1.size()>0)
							{
								type = 1;
							}
						}
					}
				}
				
				List<Equip> eList =cg.getEquips(i);
				HashMap<Integer, Equip> equipMap =new HashMap<Integer, Equip>();
				if(eList !=null)
				{
					for(int j=0;j<eList.size();j++)
					{
						if(eList.get(j)!=null)
						{
							EquipData eData =EquipData.getData(eList.get(j).getEquipId());
							if(eData !=null)
							{
								equipMap.put(eData.type, eList.get(j));
							}
						}
					}
				}
				//判断是否有空位
				if(type ==0)
				{
					if(eList !=null && eList.size()<3)//身上有装备但是有空位
					{
						if(pes2 !=null && pes2.size()>0)
						{
							for(int k=0;k<pes2.size();k++)
							{
								if(pes2.get(k)!=null)
								{
									EquipData equipData =EquipData.getData(pes2.get(k).getDataId());
								 	if(!equipMap.containsKey(equipData.type))
									{
										type =1;
										break;
									}
								}
							}
						}
					}
					else if(eList ==null || eList.size()==0)//无装备
					{
						if(pes2 !=null && pes2.size()>0)
						{
							type =1;
						}
					}
				}
				
				if(type ==0)//提升
				{
					if(eList !=null && eList.size()>0)
					{
						for(int m=0;m<eList.size();m++)
						{
							EquipData eData =EquipData.getData(eList.get(m).getEquipId());
							for(int k=0;k<pes2.size();k++)
							{
								PackElement pelement =pes2.get(k);
								EquipData equipData =EquipData.getData(pelement.getDataId());
								if(eData !=null && equipData !=null)
								{
									if(eData.type ==equipData.type && eData.star<equipData.star)
									{
										type =2;
										break;
									}
								}
							}
						}
					}
				}
				/**-------------end----------------**/
				pej.type =type;
				cs.add(pej);
				ics.add(i+"");
			}
		}
		
	}



	public List<PackElementJson> getCs() {
		return cs;
	}

	public void setCs(List<PackElementJson> cs) {
		this.cs = cs;
	}

	public List<PackElement> getSs()
	{
		return ss;
	}

	public void setSs(List<PackElement> ss)
	{
		this.ss = ss;
	}

	public List<CGPJson> getPs()
	{
		return ps;
	}

	public void setPs(List<CGPJson> ps)
	{
		this.ps = ps;
	}

	public List<CGEJson> getEs()
	{
		return es;
	}

	public void setEs(List<CGEJson> es)
	{
		this.es = es;
	}

	public List<String> getIcs()
	{
		return ics;
	}

	public void setIcs(List<String> ics)
	{
		this.ics = ics;
	}

	public List<String> getIss()
	{
		return iss;
	}

	public void setIss(List<String> iss)
	{
		this.iss = iss;
	}

	public List<String> getIps()
	{
		return ips;
	}

	public void setIps(List<String> ips)
	{
		this.ips = ips;
	}

	public List<String> getIes()
	{
		return ies;
	}

	public void setIes(List<String> ies)
	{
		this.ies = ies;
	}

	public int getUnit() {
		return unit;
	}

	public void setUnit(int unit) {
		this.unit = unit;
	}

	public int getUnitId()
	{
		return unitId;
	}

	public void setUnitId(int unitId)
	{
		this.unitId = unitId;
	}
	
}
