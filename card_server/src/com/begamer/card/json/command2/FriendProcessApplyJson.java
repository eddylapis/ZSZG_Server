package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class FriendProcessApplyJson extends BasicJson
{
	//==1同意,2拒绝==//
	public int t;
	public int i;

	public int getT()
	{
		return t;
	}

	public void setT(int t)
	{
		this.t = t;
	}

	public int getI()
	{
		return i;
	}

	public void setI(int i)
	{
		this.i = i;
	}
	
}
