package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;

public class BreakResultJson extends ErrorJson {
	public List<PackElement> pes;
	public PackElement pe;//突破卡
	public int pd;// 玩家拥有的金罡心
	public int d;// 突破需要金罡心数量
	public int cn;// 突破需要卡牌数量
	public int pcn;// 玩家拥有此卡牌的数量
	public int pmc;// 玩家拥有同星级的万能突破卡
	public int sell;//新手标识
	
	public List<PackElement> getPes()
	{
		return pes;
	}
	
	public void setPes(List<PackElement> pes)
	{
		this.pes = pes;
	}
	
	public int getPd()
	{
		return pd;
	}
	
	public void setPd(int pd)
	{
		this.pd = pd;
	}
	
	public int getD()
	{
		return d;
	}
	
	public void setD(int d)
	{
		this.d = d;
	}
	
	public int getCn()
	{
		return cn;
	}
	
	public void setCn(int cn)
	{
		this.cn = cn;
	}
	
	public int getPcn()
	{
		return pcn;
	}
	
	public void setPcn(int pcn)
	{
		this.pcn = pcn;
	}
	
	public int getPmc()
	{
		return pmc;
	}
	
	public void setPmc(int pmc)
	{
		this.pmc = pmc;
	}

	public PackElement getPe()
	{
		return pe;
	}

	public void setPe(PackElement pe)
	{
		this.pe = pe;
	}

	public int getSell()
	{
		return sell;
	}

	public void setSell(int sell)
	{
		this.sell = sell;
	}
	
}
