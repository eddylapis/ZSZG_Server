package com.begamer.card.model.dao;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.begamer.card.model.pojo.Manager;

public class ManagerLoginDao extends HibernateDaoSupport {

	private Logger logger = Logger.getLogger(ManagerLoginDao.class);

	public Manager login(String name, String password)
	{
		logger.info("finding Manager By name,password instance");
		Session session = getSession();
		try
		{
			String hql = "from Manager m where m.name=? and m.password=?";
			Query query = session.createQuery(hql);
			query.setString(0, name);
			query.setString(1, password);
			Manager loginJson = (Manager) query.uniqueResult();
			logger.info("find Manager by name,password successful");
			return loginJson;
		}
		catch (RuntimeException e)
		{
			logger.debug("find Manager by name,password failed");
			throw e;
		}
		finally
		{
			session.close();
		}
	}

}
