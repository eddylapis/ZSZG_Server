package com.begamer.card.model.pojo;


/**
 * @ClassName: Key
 * @Description:激活码
 * @author gs
 * @date 2014-5-5 下午05:10:56
 */
public class Key
{
	private int id;
	private String code;// 激活码
	private int giftId;// 礼包id
	private int serverId;// 服务器ID
	private int userId;// userId
	private int playerId;// playerid
	private int status; // 领取标识：0 未领取;1 领取
	private String receiveTime;//领取时间
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public int getGiftId()
	{
		return giftId;
	}
	public void setGiftId(int giftId)
	{
		this.giftId = giftId;
	}
	public int getServerId()
	{
		return serverId;
	}
	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}
	public int getUserId()
	{
		return userId;
	}
	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	public int getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}
	public int getStatus()
	{
		return status;
	}
	public void setStatus(int status)
	{
		this.status = status;
	}
	public String getReceiveTime()
	{
		return receiveTime;
	}
	public void setReceiveTime(String receiveTime)
	{
		this.receiveTime = receiveTime;
	}
}
